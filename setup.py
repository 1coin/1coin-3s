from distutils.core import setup, Extension

onecoin_3s_module = Extension('onecoin_3s',
                               sources = ['3smodule.c',
                                          '3s.c',
										  'sha3/simd.c',
										  'sha3/shavite.c',
  										  'sha3/skein.c'],
                               include_dirs=['.', './sha3'])

setup (name = 'onecoin_3s',
       version = '1.0',
       description = 'Bindings for proof of work used by 1coin',
       ext_modules = [onecoin_3s_module])
